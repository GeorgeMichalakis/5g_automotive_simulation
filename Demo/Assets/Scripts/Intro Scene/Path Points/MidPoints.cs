﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MidPoints : MonoBehaviour
{
    public static float _sphereRadius = 100f;
    public GameObject[] _targetStartPoints;
    [System.Serializable]
    public class TurnPoints
    {
        public Vector3[] points;
    }
    public TurnPoints[] startTargets;
    public GameObject StartPointDirectlyBehindMe;

    public static GameObject[] StartPointsInRange(Transform midPointRef,GameObject StartPointDirectlyBehind,string tag)
    {
        LayerMask layer = LayerMask.NameToLayer("Path_Start");
        int layerMask = 1 << layer.value;
        Collider[] gameObjects = Physics.OverlapSphere(midPointRef.position, _sphereRadius, layerMask);
        List<GameObject> listToBeRetuned = new List<GameObject>();
        foreach (Collider collider in gameObjects) if (collider.gameObject.CompareTag(tag) && collider.gameObject!= StartPointDirectlyBehind) listToBeRetuned.Add(collider.gameObject);
        return listToBeRetuned.ToArray();
    }

    public static TurnPoints[] GenerateTurns (GameObject car,GameObject midPoint, GameObject[] startPoints)
    {
        List<TurnPoints> turnPoints = new List<TurnPoints>();
        for (int i=0;i<startPoints.Length;i++)
        {
            TurnPoints newTurnPointArrayOfArrays = new TurnPoints();
            newTurnPointArrayOfArrays.points=CatMullRom_Generator.CatmulRom(ProvideTheControlPoints(car, midPoint, startPoints[i])).ToArray();
            turnPoints.Add(newTurnPointArrayOfArrays);
        }
        return turnPoints.ToArray();
    }

    public static Transform[] ProvideTheControlPoints(GameObject car, GameObject midPoint, GameObject startPoint)
    {
        Transform[] listToBeReturned = new Transform[5];

        Vector3 positionOfIntersectionPoint = new Vector3();
        Vector3 positionClosestToStart = new Vector3();
        Vector3 positionClosestToMidPoint = new Vector3();

        float percentage = 0.9f;
        float offset = 2f;

        if (Mathf.Abs(car.transform.forward.z)<0.5f)
        {
            positionOfIntersectionPoint = new Vector3(startPoint.transform.position.x, startPoint.transform.position.y, midPoint.transform.position.z);
            positionClosestToMidPoint = new Vector3((midPoint.transform.position.x + positionOfIntersectionPoint.x) / 2f, startPoint.transform.position.y, midPoint.transform.position.z);
            positionClosestToStart = new Vector3(startPoint.transform.position.x, startPoint.transform.position.y, (positionOfIntersectionPoint.z + startPoint.transform.position.z) / 2f);

            positionOfIntersectionPoint.z = (positionClosestToStart.z) + (DetermineTheDirection(midPoint.transform.position.z, startPoint.transform.position.z)) * Mathf.Abs(positionOfIntersectionPoint.z - positionClosestToStart.z) * percentage;
            positionClosestToStart.x -= DetermineTheDirection(midPoint.transform.position.x, startPoint.transform.position.x) * offset;
        }
        else if (Mathf.Abs(car.transform.forward.x)<0.5f)
        {
            positionOfIntersectionPoint = new Vector3(midPoint.transform.position.x, startPoint.transform.position.y, startPoint.transform.position.z);
            positionClosestToMidPoint = new Vector3(midPoint.transform.position.x, startPoint.transform.position.y, (midPoint.transform.position.z + positionOfIntersectionPoint.z) / 2f);
            positionClosestToStart = new Vector3((positionOfIntersectionPoint.x + startPoint.transform.position.x) / 2f, startPoint.transform.position.y, startPoint.transform.position.z);

            positionOfIntersectionPoint.x = (positionClosestToStart.x) + (DetermineTheDirection(midPoint.transform.position.x, startPoint.transform.position.x)) * Mathf.Abs(positionOfIntersectionPoint.x - positionClosestToStart.x) * percentage;
            positionClosestToStart.z -= DetermineTheDirection(midPoint.transform.position.z, startPoint.transform.position.z) * offset;
        }
        GameObject Temp1 = Instantiate(midPoint, positionClosestToMidPoint, Quaternion.identity);
        GameObject Temp2 = Instantiate(midPoint, positionOfIntersectionPoint, Quaternion.identity);
        GameObject Temp3 = Instantiate(midPoint, positionClosestToStart, Quaternion.identity);

        listToBeReturned[0] = midPoint.transform;
        listToBeReturned[1] = Temp1.transform;
        listToBeReturned[2] = Temp2.transform;
        listToBeReturned[3] = Temp3.transform;
        listToBeReturned[4] = startPoint.transform;

        Destroy(Temp1);
        Destroy(Temp2);
        Destroy(Temp3);

        return listToBeReturned;
    }

    private static float DetermineTheDirection(float first_node, float second_node)
    {
        if (first_node > second_node) return 1f;
        else return -1f;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position, _sphereRadius);
    }
}
