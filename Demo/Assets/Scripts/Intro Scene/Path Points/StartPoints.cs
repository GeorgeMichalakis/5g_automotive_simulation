﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartPoints : MonoBehaviour
{
    [HideInInspector]
    public GameObject[] _targetMidPoints;
    [HideInInspector]
    public GameObject _sideStartPoint;
    [HideInInspector]
    public GameObject _sideTargetMidPoint;
    [HideInInspector]
    public GameObject _nearestCar;
    [HideInInspector]
    public GameObject _firstMidPoint;

    public static GameObject FindSideStartPoint(GameObject refStartPoint)
    {
        String tag;
        switch (refStartPoint.tag)
        {
            case "Lane1":
                tag = "Lane2";
                break;
            case "Lane2":
                tag = "Lane1";
                break;
            case "Lane3":
                tag = "Lane4";
                break;
            case "Lane4":
                tag = "Lane3";
                break;
            default:
                tag="";
                break;
        }
        return Helper.FindNearestGameObjectFromSphereCollider(refStartPoint, tag, "Path_Start");
    }
    public static GameObject[] AssignTargetMidPoints(GameObject car, GameObject startPoint)
    {
        List<GameObject> midPoints = new List<GameObject>();
        LayerMask midPointLayer = LayerMask.NameToLayer("Path_MidPoint");
        LayerMask buildingsLayer = LayerMask.NameToLayer("Buildings");
        int midPointLayerMask = 1 << midPointLayer.value;
        int buildingsLayerMask = 1 << buildingsLayer.value;
        int finalMask = midPointLayerMask | buildingsLayerMask;
        AddPointsStraightAhead(ref midPoints, car.transform, startPoint.transform,startPoint.tag, finalMask);
        return midPoints.ToArray();
    }

    private static void AddPointsStraightAhead(ref List<GameObject> midPointList, Transform car, Transform refPoint, String tag,int finalMask)
    {
        if (Physics.Raycast(refPoint.transform.position, car.transform.forward, out RaycastHit hit, Mathf.Infinity, finalMask))
        {
            if (hit.collider.gameObject.CompareTag(tag))
            {
                midPointList.Add(hit.collider.gameObject);
                AddPointsStraightAhead(ref midPointList, car, hit.collider.transform, tag, finalMask);
            }
        }
    }
}
