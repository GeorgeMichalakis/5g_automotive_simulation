﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Register_PathPoints : MonoBehaviour
{
    public GameObject _paths;
    public void AssignPossiblePaths()
    {
        LayerMask midPointlayer = LayerMask.NameToLayer("Path_MidPoint");
        LayerMask startPointlayer = LayerMask.NameToLayer("Path_Start");

        foreach (Transform child in _paths.transform)
        {
            if (child.gameObject.layer == startPointlayer) child.gameObject.AddComponent<StartPoints>();
            else if (child.gameObject.layer == midPointlayer) child.gameObject.AddComponent<MidPoints>();
        }
        foreach (Transform child in _paths.transform)
        {
            if (child.gameObject.layer == startPointlayer)
            {
                GameObject car = Helper.FindNearestGameObjectFromSphereCollider(child.gameObject, "Car", "Car");
                child.gameObject.GetComponent<StartPoints>()._nearestCar = car;
                child.gameObject.GetComponent<StartPoints>()._targetMidPoints = StartPoints.AssignTargetMidPoints(car, child.gameObject);
                child.gameObject.GetComponent<StartPoints>()._firstMidPoint = child.gameObject.GetComponent<StartPoints>()._targetMidPoints[0];
                child.gameObject.GetComponent<StartPoints>()._sideStartPoint = StartPoints.FindSideStartPoint(child.gameObject);
                child.gameObject.GetComponent<StartPoints>()._firstMidPoint.GetComponent<MidPoints>().StartPointDirectlyBehindMe = child.gameObject;
            }
        }
        foreach (Transform child in _paths.transform)
        {
            if (child.gameObject.layer == midPointlayer)
            {
                child.gameObject.GetComponent<MidPoints>()._targetStartPoints = MidPoints.StartPointsInRange(child.gameObject.transform, child.gameObject.GetComponent<MidPoints>().StartPointDirectlyBehindMe, child.gameObject.tag);
            }
        }
        foreach (Transform child in _paths.transform)
        {
            if (child.gameObject.layer == startPointlayer)
            {
                child.gameObject.GetComponent<StartPoints>()._sideTargetMidPoint = child.gameObject.GetComponent<StartPoints>()._sideStartPoint.GetComponent<StartPoints>()._targetMidPoints[0];
            }
        }
        foreach (Transform child in _paths.transform)
        {
            if (child.gameObject.layer == midPointlayer)
            {
                child.gameObject.GetComponent<MidPoints>().startTargets = MidPoints.GenerateTurns(child.gameObject.GetComponent<MidPoints>().StartPointDirectlyBehindMe.GetComponent<StartPoints>()._nearestCar, child.gameObject, child.gameObject.GetComponent<MidPoints>()._targetStartPoints);
            }
        }
    }
}
