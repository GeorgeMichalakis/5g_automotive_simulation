﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopSignConfiguration : MonoBehaviour
{
    public float _totalDuration;
    public float _stopColliderDuration;
    public bool Stop;
    private float t = 0f;
    void Update()
    {
        t += Time.deltaTime;
        if (t > _totalDuration) t = 0f;
        else
        {
            if (t < _stopColliderDuration) Stop = true;
            else Stop = false;
        }
    }
}
