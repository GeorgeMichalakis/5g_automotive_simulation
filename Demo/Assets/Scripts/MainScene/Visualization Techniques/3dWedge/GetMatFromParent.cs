﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetMatFromParent : MonoBehaviour
{
    void Start()
    {
        Color parentcolor = transform.parent.GetComponent<MeshRenderer>().material.color;
        transform.GetComponent<MeshRenderer>().material.color = new Color(parentcolor.r,parentcolor.g,parentcolor.b, transform.GetComponent<MeshRenderer>().material.color.a);
    }
}
