﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindScreenCollider : MonoBehaviour
{
    public List<GameObject> gameObjectsWhichAmCollidingWith = new List<GameObject>();

    private void OnTriggerEnter(Collider other)
    {
        if (!gameObjectsWhichAmCollidingWith.Contains(other.gameObject.transform.root.gameObject)) gameObjectsWhichAmCollidingWith.Add(other.gameObject.transform.root.gameObject);
    }

    private void OnTriggerExit(Collider other)
    {
        if (gameObjectsWhichAmCollidingWith.Contains(other.gameObject.transform.root.gameObject)) gameObjectsWhichAmCollidingWith.Remove(other.gameObject.transform.root.gameObject);
    }
}
