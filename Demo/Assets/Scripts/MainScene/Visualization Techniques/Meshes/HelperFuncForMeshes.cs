﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HelperFuncForMeshes
{
    private static int _carLayer=LayerMask.NameToLayer("Car");
    private static int _buildingsLayer = LayerMask.NameToLayer("Buildings");
    public static void CarsAroundMe (ref List<GameObject> carsBeingMonitored, Transform car, Transform referenceDriver,float thresholdtoRender,GameObject mainCamera,Shader standardShader,Shader overlayShader,bool windScreenOnly,GameObject windScreen,bool regularMeshes)
    {
        int layerMask = 1 << _carLayer;
        int layerMaskBuildings = 1 << _buildingsLayer;
        int layerMaskCarOrBuildings = layerMask | layerMaskBuildings;
        Collider[] nearbyCars = Physics.OverlapSphere(referenceDriver.position, thresholdtoRender, layerMask);
        foreach (Collider carCollid in nearbyCars)
        {
            GameObject carCollider = carCollid.transform.root.gameObject;
            if (carCollider.gameObject != car.gameObject)
            {
                if (!carsBeingMonitored.Contains(carCollider.gameObject))
                {
                    if (Physics.Raycast(mainCamera.transform.position, carCollider.gameObject.transform.position - mainCamera.transform.position, out RaycastHit hit, Mathf.Infinity, layerMaskCarOrBuildings))
                    {
                        if (hit.collider.gameObject.layer == _buildingsLayer)
                        {
                            carsBeingMonitored.Add(carCollider.gameObject);
                            //if (windScreenOnly)
                            //    if (windScreen.GetComponent<WindScreenCollider>().gameObjectsWhichAmCollidingWith.Contains(carCollider.gameObject))
                            if (!regularMeshes)
                            {
                                foreach (Transform child in carCollider.gameObject.transform.GetChild(2).transform)
                                {
                                    child.gameObject.GetComponent<MeshRenderer>().enabled = false;
                                }
                                carCollider.gameObject.transform.GetChild(2).transform.GetChild(1).gameObject.GetComponent<MeshRenderer>().enabled = true;
                                carCollider.gameObject.transform.GetChild(2).transform.GetChild(1).gameObject.GetComponent<MeshFilter>().mesh=GameObject.CreatePrimitive(PrimitiveType.Sphere).GetComponent<MeshFilter>().mesh;
                                carCollider.gameObject.transform.GetChild(2).transform.GetChild(1).transform.GetComponent<MeshRenderer>().material.shader = overlayShader;
                            }
                            else
                            {
                                carCollider.gameObject.transform.GetChild(2).transform.GetChild(0).transform.GetComponent<MeshRenderer>().material.shader = overlayShader;
                                carCollider.GetComponent<Collider>().gameObject.GetComponentInChildren<AnnotationObjects>().displayAnnotations = true;
                            }
                        }
                        else
                        {
                            carCollider.gameObject.transform.GetChild(2).transform.GetChild(0).transform.GetComponent<MeshRenderer>().material.shader = standardShader;
                            carCollider.GetComponent<Collider>().gameObject.GetComponentInChildren<AnnotationObjects>().displayAnnotations = true;
                        }
                    }
                }
            }
        }
    }

    public static void CheckEveryCarInsideList(ref List<GameObject> carsBeingMonitored, Transform referenceDriver, float _thresholdtoRender, GameObject mainCamera, Shader standardShader)
    {
        int layerMask = 1 << _carLayer;
        int layerMaskBuildings = 1 << _buildingsLayer;
        int layerMaskCarOrBuildings = layerMask | layerMaskBuildings;
        try
        {
            foreach (GameObject car in carsBeingMonitored)
            {

                bool removeIt = true;
                if (Vector3.Distance(referenceDriver.position, car.transform.position) <= _thresholdtoRender)
                {
                    if (Physics.Raycast(mainCamera.transform.position, car.transform.position - mainCamera.transform.position, out RaycastHit hit, layerMaskCarOrBuildings))
                    {
                        if (hit.collider.gameObject.layer == _buildingsLayer) removeIt = false;
                        else removeIt = true;
                    }
                }
                if (removeIt)
                {
                    car.gameObject.transform.GetChild(2).transform.GetChild(0).transform.GetComponent<MeshRenderer>().material.shader = standardShader;
                    car.gameObject.GetComponentInChildren<AnnotationObjects>().displayAnnotations = false;
                    carsBeingMonitored.Remove(car);
                }
            }
        }
        catch {; }
    }
}
