﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateLengthAndRotation : MonoBehaviour
{
    public Transform refTransform;
    public GameObject _objectToFollow;
    private float limitToShow=50f;
    public float _prevDist;
    private float t;
    void Update()
    {
        try
        {
            Vector3 _dist = refTransform.position - _objectToFollow.transform.position;
            _dist.y = 0;
            transform.localScale = new Vector3(transform.localScale.x,refTransform.localScale.y, _dist.magnitude/limitToShow);
            transform.LookAt(_objectToFollow.transform);
            _prevDist = refTransform.InverseTransformPoint(_objectToFollow.transform.position).magnitude;
        }
        catch { ; }
    }

}
