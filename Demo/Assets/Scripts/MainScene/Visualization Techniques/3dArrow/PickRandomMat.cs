﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickRandomMat : MonoBehaviour
{
    public MeshRenderer carMeshRenderer;
    private Color[] colors = new Color[6];
    void Start()
    {
        colors[0] = Color.red;
        colors[1] = Color.blue;
        colors[2] = new Color(248, 24, 148);
        colors[3] = Color.yellow;
        colors[4] = Color.grey;
        colors[5] = Color.magenta;
        if (carMeshRenderer == null) GetComponent<MeshRenderer>().material.color = colors[Random.Range(0, colors.Length)];
        else carMeshRenderer.material.color = colors[Random.Range(0, colors.Length)];
    }
}
