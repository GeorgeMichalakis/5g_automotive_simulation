﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using UnityEditor;
using UnityEngine;

public class RenderTechnique : MonoBehaviour
{
    public UseVisualizationTechnique.VisualizationTechnique _selectedTechnique;
    public GameObject _car;
    public GameObject _arrow3D;
    public GameObject _3dWedge;
    public Transform _referenceToSpawnSphereFrom;
    public float _thresholdtoRender;
    private int _carLayer;
    private int _buildingsLayer;
    private List<GameObject> carsBeingMonitored = new List<GameObject>();
    private List<GameObject> Arrows3D = new List<GameObject>();

    public GameObject mainCamera;
    private void Awake()
    {
        _carLayer = LayerMask.NameToLayer("Car");
        _buildingsLayer = LayerMask.NameToLayer("Buildings");
    }
    private void Update()
    {
        if (_selectedTechnique == UseVisualizationTechnique.VisualizationTechnique.Arrow3D) EnableArrows(_arrow3D, _car.transform, _referenceToSpawnSphereFrom);
        else if(_selectedTechnique == UseVisualizationTechnique.VisualizationTechnique.Wedge3D) EnableArrows(_3dWedge, _car.transform, _referenceToSpawnSphereFrom);
        CheckEveryCarInsideList(ref carsBeingMonitored, _referenceToSpawnSphereFrom);
        GetComponent<MeshRenderer>().enabled = carsBeingMonitored.Count != 0;
    }
    private void EnableArrows(GameObject arrow3D, Transform car, Transform referenceDriver)
    {
        int layerMask = 1 << _carLayer;
        int layerMaskBuildings = 1 << _buildingsLayer;
        int layerMaskCarOrBuildings = layerMask | layerMaskBuildings;
        Collider[] nearbyCars = Physics.OverlapSphere(referenceDriver.position, _thresholdtoRender, layerMask);
        foreach (Collider carCollid in nearbyCars)
        {
            GameObject carCollider = carCollid.transform.root.gameObject;
            if (carCollider.gameObject != car.gameObject)
            {
                if (!carsBeingMonitored.Contains(carCollider.gameObject))
                {
                    if (Physics.Raycast(mainCamera.transform.position, carCollider.gameObject.transform.position - mainCamera.transform.position, out RaycastHit hit,Mathf.Infinity, layerMaskCarOrBuildings))
                    {
                        if (hit.collider.gameObject.layer==_buildingsLayer)
                        {
                            carsBeingMonitored.Add(carCollider.gameObject);
                            GameObject arrow = Instantiate(arrow3D, transform);
                            arrow.GetComponent<UpdateLengthAndRotation>()._objectToFollow = carCollider.gameObject;
                            arrow.GetComponent<UpdateLengthAndRotation>().refTransform = referenceDriver;
                            Arrows3D.Add(arrow);
                        }
                    }
                }
            }
        }
    }

    private void CheckEveryCarInsideList(ref List<GameObject> carsBeingMonitored, Transform referenceDriver)
    {
        int layerMask = 1 << _carLayer;
        int layerMaskBuildings = 1 << _buildingsLayer;
        int layerMaskCarOrBuildings = layerMask | layerMaskBuildings;
        foreach (GameObject car in carsBeingMonitored.ToList())
        {
            bool removeIt = true;
            if (Vector3.Distance(referenceDriver.position, car.transform.position) <= _thresholdtoRender)
            {
                if (Physics.Raycast(mainCamera.transform.position, car.transform.position - mainCamera.transform.position, out RaycastHit hit, layerMaskCarOrBuildings))
                {
                    if (hit.collider.gameObject.layer == _buildingsLayer) removeIt = false;
                    else removeIt = true;
                }
            }
            if (removeIt)
            {
                foreach (GameObject arrow in Arrows3D.ToList())
                {
                    try
                    {
                        if (car != null && arrow.GetComponent<UpdateLengthAndRotation>()._objectToFollow == car && arrow != null)
                        {
                            Destroy(arrow);
                            carsBeingMonitored.Remove(car);
                        }
                    }
                    catch {; }
                }
            }
        }
    }
}
