﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowRotation : MonoBehaviour 
{
    public GameObject _car;
    public GameObject _hmd;
    public GameObject Heading;
    public float _offsetZ;
    public float _offsetY;
    void Update()
    {
        Vector3 pos = _hmd.transform.position + _hmd.transform.forward * _offsetZ + _hmd.transform.up * (-_offsetY);
        transform.position = pos;
        transform.rotation = _hmd.transform.rotation;
        Vector3 Angle = _hmd.transform.localRotation.eulerAngles;
        Heading.transform.position = transform.position;
        Heading.transform.rotation = transform.rotation;
        Heading.transform.eulerAngles = new Vector3(Heading.transform.eulerAngles.x, Heading.transform.eulerAngles.y, -Angle.y);
    }
}
