﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSpawner : MonoBehaviour
{
    #region Attributes
    [Tooltip("Pick how many vehicles you want")]
    [Range(1, 76)]
    public int _amountOfVehicles;
    [Tooltip("Assign the parent of Spawn Points")]
    public GameObject _spawnPoints;
    [Tooltip("Assign the parent of Path Points")]
    public GameObject _pathPoints;
    [Tooltip("Assign Car Prefab")]
    public GameObject Car;
    #endregion
    private void Awake()
    {
        SpawnTheCars();
    }
    #region Public Methods
    public void SpawnTheCars()
    {
        //Debug.Log(_spawnPoints.transform.childCount);
        int[] _ChoosedSpawnPoints = PickPseudoRandomIndexesToBeSpawned(_amountOfVehicles, _spawnPoints);
        int i;
        GameObject[] spawnPoints = new GameObject[_spawnPoints.transform.childCount];
        GameObject[] pathPoints = new GameObject[_pathPoints.transform.childCount];
        for (i = 0; i < spawnPoints.Length; i++) spawnPoints[i] = _spawnPoints.transform.GetChild(i).gameObject;
        for (i = 0; i < pathPoints.Length; i++) pathPoints[i] = _pathPoints.transform.GetChild(i).gameObject;

        for (i = 0; i < _ChoosedSpawnPoints.Length; i++)
        {
            GameObject refGameObject = spawnPoints[_ChoosedSpawnPoints[i]];
            GameObject nearestStartPoint = Helper.FindClosestGameObject(pathPoints, refGameObject, "Path_Start");
            GameObject car = Instantiate(Car, spawnPoints[_ChoosedSpawnPoints[i]].transform.position, Quaternion.identity);
            car.transform.LookAt(nearestStartPoint.transform);
            car.GetComponent<Data>().CurrentLane = spawnPoints[_ChoosedSpawnPoints[i]].tag;
        }

    }
    #endregion

    #region Private Methods
    private int[] PickPseudoRandomIndexesToBeSpawned(int amountOfVehicles, GameObject parent)
    {
        List<int> initArray = new List<int>();
        int i;
        for (i = 0; i < parent.transform.childCount; i++) initArray.Add(i);
        if (amountOfVehicles < parent.transform.childCount)
        {
            int[] newArray = new int[amountOfVehicles];
            for (i = 0; i < amountOfVehicles; i++)
            {
                int index = UnityEngine.Random.Range(0, initArray.Count);
                newArray[i] = initArray[index];
                initArray.RemoveAt(index);
            }
            return newArray;
        }
        return initArray.ToArray();
    }
    #endregion
}
