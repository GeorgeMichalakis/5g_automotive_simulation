﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animation : MonoBehaviour
{
    public bool lightStartsFromRed;

    public float _totalDuration;
    public float _redLightDuration;
    public float _greenLightDuration;
    public float _yellowDuration;

    public bool _red = false;
    public bool _yellow= false;
    public bool _green=false;

    private float t=0f;
    /*
     * 0,1 Reds
     * 2,3 Yellows
     * 4,5 Greens
     */

    public GameObject LightParent;
    private void Start()
    {
        _red = lightStartsFromRed;
        if (!lightStartsFromRed)
        {
            _green = true;
            t = _redLightDuration+_yellowDuration;
            LightParent.transform.GetChild(4).GetComponent<Light>().enabled = true;
            LightParent.transform.GetChild(5).GetComponent<Light>().enabled = true;
        }
    }
    private void Update()
    {
        DisplayLights(_totalDuration, _redLightDuration, _yellowDuration, _greenLightDuration);
    }

    private void DisplayLights(float totalDuration,float redLightTime,float yellowLightTime,float greenLightTime)
    {
        t += Time.deltaTime;
        if (t > totalDuration) t = 0f;
        else
        {
            if (t < redLightTime)
            {
                _green = false;
                LightParent.transform.GetChild(4).GetComponent<Light>().enabled = false;
                LightParent.transform.GetChild(5).GetComponent<Light>().enabled = false;
                _red = true;
                LightParent.transform.GetChild(0).GetComponent<Light>().enabled = true;
                LightParent.transform.GetChild(1).GetComponent<Light>().enabled = true;
            }
            else if (t < redLightTime+ yellowLightTime)
            {
                _red = false;
                LightParent.transform.GetChild(0).GetComponent<Light>().enabled = false;
                LightParent.transform.GetChild(1).GetComponent<Light>().enabled = false;
                _yellow = true;
                LightParent.transform.GetChild(2).GetComponent<Light>().enabled = true;
                LightParent.transform.GetChild(3).GetComponent<Light>().enabled = true;
            }
            else
            {
                _yellow = false;
                LightParent.transform.GetChild(2).GetComponent<Light>().enabled = false;
                LightParent.transform.GetChild(3).GetComponent<Light>().enabled = false;
                _green = true;
                LightParent.transform.GetChild(4).GetComponent<Light>().enabled = true;
                LightParent.transform.GetChild(5).GetComponent<Light>().enabled = true;
            }
        }
    }
}
