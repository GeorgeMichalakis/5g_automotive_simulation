﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PedSensor : MonoBehaviour
{
    #region Attributes
    public static float sensorLength = 20f;
    public readonly float sensorLength5G = 100f;
    public readonly float sensorLength5g_NOT = 50f;

    private static readonly float[] frontSensorAngles = { 0f, 10f, 20f, 30f, 40f, 50f, 60f, 70f, 80f, 90f };
    #endregion

    #region Public Methods
    public static void ObjectScanner(ref GameObject frontObject, ref float frontObjectDistance, ref GameObject rightSideObject, ref float rightSideDistance, ref GameObject leftSideObject, ref float leftSideDistance, GameObject Car)
    {
        RaycastHit hit;
        frontObject = null;
        rightSideObject = null;
        leftSideObject = null;

        //Angle Sensor
        for (int i = 0; i < frontSensorAngles.Length; i++)
        {
            if (i < 4)
            {
                //Front Side
                if (Physics.Raycast(Car.transform.position, Quaternion.AngleAxis(frontSensorAngles[i], Car.transform.up) * Car.transform.forward, out hit, sensorLength, LayerMask.GetMask("Pedestrians") | LayerMask.GetMask("Scooter") | LayerMask.GetMask("Buildings")))
                {
                    if ((hit.collider.gameObject.CompareTag("Pedestrian") || hit.collider.gameObject.CompareTag("Scooter"))) frontObject = hit.collider.gameObject;
                }

                if (Physics.Raycast(Car.transform.position, Quaternion.AngleAxis(-frontSensorAngles[i], Car.transform.up) * Car.transform.forward, out hit, sensorLength, LayerMask.GetMask("Pedestrians") | LayerMask.GetMask("Scooter") | LayerMask.GetMask("Buildings")))
                {
                    if ((hit.collider.gameObject.CompareTag("Pedestrian") || hit.collider.gameObject.CompareTag("Scooter"))) frontObject = hit.collider.gameObject;
                }
            }
            else
            {
                //Right
                if (Physics.Raycast(Car.transform.position, Quaternion.AngleAxis(frontSensorAngles[i], Car.transform.up) * Car.transform.forward, out hit, sensorLength, LayerMask.GetMask("Pedestrians") | LayerMask.GetMask("Scooter") | LayerMask.GetMask("Buildings")))
                {
                    if ((hit.collider.gameObject.CompareTag("Pedestrian") || hit.collider.gameObject.CompareTag("Scooter"))) rightSideObject = hit.collider.gameObject;
                }
                //Left
                if (Physics.Raycast(Car.transform.position, Quaternion.AngleAxis(-frontSensorAngles[i], Car.transform.up) * Car.transform.forward, out hit, sensorLength, LayerMask.GetMask("Pedestrians") | LayerMask.GetMask("Scooter") | LayerMask.GetMask("Buildings")))
                {
                    if ((hit.collider.gameObject.CompareTag("Pedestrian") || hit.collider.gameObject.CompareTag("Scooter"))) leftSideObject = hit.collider.gameObject;
                }
            }
        }
        if (frontObject != null) frontObjectDistance = Car.transform.InverseTransformPoint(frontObject.transform.position).magnitude;
        else frontObjectDistance = 0f;
        if (rightSideObject != null) rightSideDistance = Car.transform.InverseTransformPoint(rightSideObject.transform.position).magnitude;
        else rightSideDistance = 0f;
        if (leftSideObject != null) leftSideDistance = Car.transform.InverseTransformPoint(leftSideObject.transform.position).magnitude;
        else leftSideDistance = 0f;
    }

    #endregion
}
