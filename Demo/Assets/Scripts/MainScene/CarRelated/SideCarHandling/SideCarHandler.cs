﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SideCarHandler
{
    public static void IsSideCarTurningTowardsMe(ref bool sideCarIsTurning,GameObject leftSideCar,GameObject rightSideCar,String sideLane)
    {
        if (leftSideCar != null && leftSideCar.GetComponent<Data>().CurrentLane == sideLane) sideCarIsTurning = leftSideCar.GetComponent<Data>().Turning;
        else if (rightSideCar != null && rightSideCar.GetComponent<Data>().CurrentLane == sideLane) sideCarIsTurning = rightSideCar.GetComponent<Data>().Turning;
        else sideCarIsTurning = false;
    } 

    //Lane1>Lane2 & Lane4>Lane3
    public static void DecideLanePriority(ref bool sideCarIsTurning,bool Turning,string currentLane)
    {
        if (sideCarIsTurning && Turning)
        {
            if (currentLane.Equals("Lane1") || currentLane.Equals("Lane4")) sideCarIsTurning = false;
        }
    }
}
