﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DetectTrafficLights
{
    private static readonly float limitDistanceToGo=1f;
    private static readonly float limitDistanceToStop = 100f;
    private static int TrafficLightLayer = LayerMask.GetMask("Traffic_Light");
    public static void DetectLights(ref bool redLight, Transform ReferenceTransform,bool turning)
    {
        if (Physics.Raycast(ReferenceTransform.position, ReferenceTransform.forward, out RaycastHit hit, limitDistanceToStop+1, TrafficLightLayer))
        {
            float distance = ReferenceTransform.InverseTransformPoint(hit.collider.gameObject.transform.position).magnitude;
            if (distance<limitDistanceToStop && distance>limitDistanceToGo) redLight = hit.collider.gameObject.GetComponentInParent<Animation>()._red && !turning;
            else redLight = false;
        }
        else redLight = false;
    }
}
