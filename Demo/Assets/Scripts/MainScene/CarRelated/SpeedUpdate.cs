﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedUpdate : MonoBehaviour
{
    public Data Car;
    void Update()
    {
        GetComponent<TextMesh>().text = (Car.currentSpeed).ToString("0");
    }
}
