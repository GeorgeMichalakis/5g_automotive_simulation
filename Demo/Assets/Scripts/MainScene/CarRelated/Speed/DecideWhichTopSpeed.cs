﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DecideWhichTopSpeed
{
    public static void DecideWhichSpeedToPick(ref float maxSpeed, float straightTopSpeed, float turnTopSpeed, bool IsCarTurning) { maxSpeed = IsCarTurning ? turnTopSpeed : straightTopSpeed; }
}
