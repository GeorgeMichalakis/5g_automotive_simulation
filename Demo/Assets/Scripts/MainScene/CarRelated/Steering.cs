﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Steering
{
    #region Attributes
    [Header("Steering Configurations")]
    public static float targetSteerAngle;
    public static float maxSteerAngle =30f;
    private static readonly float AntiRoll = 5000.0f;

    private static WheelHit hit;
    private static float travelL = 1f;
    private static float travelR = 1f;

    private static bool groundedL0;
    private static bool groundedR0;

    private static bool groundedL1;
    private static bool groundedR1;

    private static float antiRollForce;
    #endregion

    #region Public Methods

    public static void AntiRollBars(ref Rigidbody rbody,WheelCollider wheelFrontLeft, WheelCollider wheelFrontRight, WheelCollider wheelRearLeft, WheelCollider wheelRearRight)
    {
        groundedL0 = wheelFrontLeft.GetGroundHit(out hit);
        groundedR0 = wheelFrontRight.GetGroundHit(out hit);
        if (groundedL0) travelL = (-wheelFrontLeft.transform.InverseTransformPoint(hit.point).y - wheelFrontLeft.radius) / wheelFrontLeft.suspensionDistance;
        if (groundedR0) travelR = (-wheelFrontRight.transform.InverseTransformPoint(hit.point).y - wheelFrontRight.radius) / wheelFrontRight.suspensionDistance;
        antiRollForce = (travelL - travelR) * AntiRoll;

        if (groundedL0) rbody.AddForceAtPosition(wheelFrontLeft.transform.up * -antiRollForce, wheelFrontLeft.transform.position);
        if (groundedR0) rbody.AddForceAtPosition(wheelFrontRight.transform.up * antiRollForce, wheelFrontRight.transform.position);

        groundedL1 = wheelRearLeft.GetGroundHit(out hit);
        groundedR1 = wheelRearRight.GetGroundHit(out hit);
        if (groundedL1) travelL = (-wheelRearLeft.transform.InverseTransformPoint(hit.point).y - wheelRearLeft.radius) / wheelRearLeft.suspensionDistance;
        if (groundedR1) travelR = (-wheelRearRight.transform.InverseTransformPoint(hit.point).y - wheelRearRight.radius) / wheelRearRight.suspensionDistance;
        antiRollForce = (travelL - travelR) * AntiRoll;

        if (groundedL1) rbody.AddForceAtPosition(wheelRearLeft.transform.up * -antiRollForce, wheelRearLeft.transform.position);
        if (groundedR1) rbody.AddForceAtPosition(wheelRearRight.transform.up * antiRollForce, wheelRearRight.transform.position);

    }
    public static void ApplySteer(ref WheelCollider wheelFrontLeft, ref WheelCollider wheelFrontRight,GameObject Reference,GameObject car, GameObject TargetNode,List<Vector3> splinePoints,int currentIndex, bool Auto,bool Turning)
    {
        if (Auto)
        {
            Vector3 relativeVector;
            if (Turning)
            {
                relativeVector = Reference.transform.InverseTransformPoint(splinePoints[currentIndex].x, car.transform.position.y, splinePoints[currentIndex].z);
                float newSteer = (relativeVector.x / relativeVector.magnitude) * maxSteerAngle;
                targetSteerAngle = newSteer;
            }
            else
            {

                relativeVector = Reference.transform.InverseTransformPoint(TargetNode.transform.position.x, car.transform.position.y, TargetNode.transform.position.z);
                float newSteer = (relativeVector.x / relativeVector.magnitude) * maxSteerAngle;
                targetSteerAngle = newSteer;
            }
        }
        else
        {
            float newSteer = Input.GetAxis("Horizontal") * maxSteerAngle;
            targetSteerAngle = newSteer;
        }
        wheelFrontLeft.steerAngle = targetSteerAngle;
        wheelFrontRight.steerAngle = targetSteerAngle;
    }
    #endregion
}
