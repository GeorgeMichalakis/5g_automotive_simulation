﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExceedingManager
{
    public static void ExceedingMaxSpeed(ref bool exceedingSpeed,float currentSpeed,float maxSpeed) { exceedingSpeed = currentSpeed > maxSpeed; }
}
