﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringWheel : MonoBehaviour
{
    public Data Car;

    void Update()
    {
        transform.localRotation = Quaternion.Euler(15, 0, -Car.wheelFrontLeft.steerAngle);
    }
}
