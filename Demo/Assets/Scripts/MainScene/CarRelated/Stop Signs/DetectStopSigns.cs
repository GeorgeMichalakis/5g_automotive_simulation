﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DetectStopSigns
{
    private static readonly float limitDistanceToGo = 1f;
    private static readonly float limitDistanceToStop = 20f;
    private static int StopSignLayer = LayerMask.GetMask("Stop_Sign");
    public static void DetectStopSign(ref bool stopSign,Transform Reference,bool Turning)
    {
        if (Physics.Raycast(Reference.position, Reference.forward, out RaycastHit hit, limitDistanceToStop + 1, StopSignLayer))
        {
            if (Reference.transform.InverseTransformPoint(hit.collider.gameObject.transform.position).magnitude < limitDistanceToStop && Reference.transform.InverseTransformPoint(hit.collider.gameObject.transform.position).magnitude > limitDistanceToGo)
            {
                if (!Turning) stopSign = (hit.collider.gameObject.CompareTag("StopSign") && hit.collider.gameObject.GetComponentInParent<StopSignConfiguration>().Stop) || (hit.collider.gameObject.CompareTag("StopSignOppositeLayer") && !hit.collider.gameObject.GetComponentInParent<StopSignConfiguration>().Stop);
            }
            else stopSign = false;
        }
        else stopSign = false;
    }
}
