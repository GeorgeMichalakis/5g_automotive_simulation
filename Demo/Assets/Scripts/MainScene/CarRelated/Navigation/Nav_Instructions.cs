﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*Auto: 5G-Help
 * Navigation
 * Warning signs (Red) (Same panel just below or up)
 */
public class Nav_Instructions : MonoBehaviour
{
    public Data dataScript;
    public bool erase = false;

    private TextMesh text;
    private string[] instructions = {"Turn Left", "Turn Right", "Straight ahead" };
    void Start()
    {
        text = GetComponent<TextMesh>();
        text.text = "";
    }

    private void Update()
    {
        Warning();
        UpdateNavigationBoard();
        DisplayText(instructions[0], erase);
    }

    private void Warning()
    {
        throw new NotImplementedException();
    }

    private void Navigation()
    {
        //GET arrows and just display them xD
    }

    private void UpdateNavigationBoard()
    {
        
    }

    private void DisplayText(string instructionToBeDisplayed, bool eraseFlag)
    {
        if (eraseFlag)
        {
            text.text = "";
            erase=ResetErase();
        }
    }

    private bool ResetErase()
    {
        return false;
    }
}
