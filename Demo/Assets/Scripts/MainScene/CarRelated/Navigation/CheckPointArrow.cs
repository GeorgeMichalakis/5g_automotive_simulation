﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointArrow : MonoBehaviour
{
    public Data script;
    public GameObject CheckPoints;
    private bool Auto;
    private List<Transform> points=new List<Transform>();
    private List<MeshRenderer> pointRender = new List<MeshRenderer>();
    private int currentCheckPointIndex = 0;
    private float limit = 10f;

    private Vector3 RemoveY = Vector3.one - Vector3.up;
    void Start()
    {
        Auto = script.Auto;
        foreach (Transform child in transform)
        {
            child.GetComponent<MeshRenderer>().enabled = !Auto;
        }
        if (!Auto)
        {
            foreach (Transform child in CheckPoints.transform)
            {
                points.Add(child);
                pointRender.Add(child.gameObject.GetComponent<MeshRenderer>());
                pointRender[currentCheckPointIndex].enabled = true;
            }
        }
    }
    private void Update()
    {
        if (!Auto)
        {
            transform.rotation = Quaternion.LookRotation(transform.position - points[currentCheckPointIndex].position);
            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
            if (script.gameObject.transform.InverseTransformPoint(points[currentCheckPointIndex].transform.position).magnitude < limit)
            {
                Quaternion prevRotation = transform.rotation;
                pointRender[currentCheckPointIndex].enabled = false;
                currentCheckPointIndex++;
                pointRender[currentCheckPointIndex].enabled = true;
                transform.rotation = Quaternion.Slerp(prevRotation, Quaternion.LookRotation(transform.position - points[currentCheckPointIndex].position), 0.5f*Time.deltaTime);
            }
        }
    }
}
