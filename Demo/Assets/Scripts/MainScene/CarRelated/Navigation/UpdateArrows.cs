﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateArrows : MonoBehaviour
{
    public Data script;
    public GameObject[] arrows= new GameObject[3];
    public Vector3 turn;
    public Vector3 axisMov;
    public float positive;
    public TextMesh textForAutoManual;

    private bool enableAuto;
    private bool CurrentState=false;
    private bool prevCurrentState;
    private void Start()
    {
        //CurrentState = script.GetComponent<TurningBehaviour>().Turning;
    }
    void Update()
    {
        enableAuto = script.Auto;
        if (script.Auto) textForAutoManual.text = "Auto";
        else textForAutoManual.text = "Manual";

        AutoNavDisplay(enableAuto);
    }

    private void AutoNavDisplay(bool enabled)
    {
        if (enabled)
        {
            //CurrentState = script.turningBehaviourScript.Turning;
            if (CurrentState != prevCurrentState)
            {
                turn = (script.targetNode.transform.position - script.gameObject.transform.position).normalized;
                if (CurrentState)
                {
                    if (axisMov == Vector3.right)
                    {
                        if (turn.x > 0)
                        {
                            //Right
                            if (turn.z < 0)
                            {
                                arrows[0].SetActive(false);
                                arrows[1].SetActive(true);
                                arrows[2].SetActive(false);
                            }
                            //Left
                            else
                            {
                                arrows[0].SetActive(false);
                                arrows[1].SetActive(false);
                                arrows[2].SetActive(true);
                            }
                        }
                        else
                        {
                            //Right
                            if (turn.z > 0)
                            {
                                arrows[0].SetActive(false);
                                arrows[1].SetActive(true);
                                arrows[2].SetActive(false);
                            }
                            //Left
                            else
                            {
                                arrows[0].SetActive(false);
                                arrows[1].SetActive(false);
                                arrows[2].SetActive(true);
                            }
                        }
                    }
                    else
                    {
                        if (turn.z > 0)
                        {
                            //Right
                            if (turn.x > 0)
                            {
                                arrows[0].SetActive(false);
                                arrows[1].SetActive(true);
                                arrows[2].SetActive(false);
                            }
                            //Left
                            else
                            {
                                arrows[0].SetActive(false);
                                arrows[1].SetActive(false);
                                arrows[2].SetActive(true);
                            }
                        }
                        else
                        {
                            //Right
                            if (turn.x < 0)
                            {
                                arrows[0].SetActive(false);
                                arrows[1].SetActive(true);
                                arrows[2].SetActive(false);
                            }
                            //Left
                            else
                            {
                                arrows[0].SetActive(false);
                                arrows[1].SetActive(false);
                                arrows[2].SetActive(true);
                            }
                        }
                    }
                }
                else
                {
                    arrows[1].SetActive(false);
                    arrows[2].SetActive(false);
                    arrows[0].SetActive(true);
                }
            }
            prevCurrentState = CurrentState;
        }
    }
}
