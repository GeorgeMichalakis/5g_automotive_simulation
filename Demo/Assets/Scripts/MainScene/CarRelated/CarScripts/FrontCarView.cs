﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrontCarView : MonoBehaviour
{
    #region Attributes
    public Data scriptData;
    public RenderTexture targetTex;

    private RenderTexture theTex;
    private GameObject frontCar;
    private new Renderer renderer;
    private Texture initTex;
    #endregion

    #region Engine Methods
    private void Start()
    {
        Init();
    }
    private void Update()
    {
        //if (scriptData.enable5G)
        //{
        //    RenderTexture();
        //}
    }
    #endregion

    #region Main Methods
    private void Init()
    {
        renderer = GetComponent<Renderer>();
        initTex = renderer.material.GetTexture("_MainTex");
    }
    private void RenderTexture()
    {
        //if (frontCar != null && (frontCar.GetComponent<Data>().CurrentLane==scriptData.CurrentLane || !scriptData.Auto) && frontCar.GetComponent<Data>().turningBehaviourScript.Turning)
        //{
        //    foreach (Transform child in frontCar.transform)
        //    {
        //        if (child.GetComponent<Camera>())
        //        {
        //            child.GetComponent<Camera>().enabled = true;
        //            child.GetComponent<Camera>().targetTexture = targetTex;
        //        }
        //    }
        //    renderer.material.SetTexture("_MainTex", theTex);

        //    theTex = frontCar.GetComponentInChildren<Camera>().targetTexture;
        //    renderer.material.SetTexture("_MainTex", theTex);
        //}
        //else renderer.material.SetTexture("_MainTex", initTex);
    }
    #endregion
}
