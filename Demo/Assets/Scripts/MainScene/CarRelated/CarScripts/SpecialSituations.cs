﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialSituations : MonoBehaviour
{
    [Header("***Situations***")]

    public bool AtStopSign = false;
    public bool TrafficLightType1 = false;
    public bool TrafficLightType2 = false;
    public bool release = false;
}
