﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class ChangeLaneBehaviour : MonoBehaviour
{
    #region Attributes
    private readonly float changeLaneDistanceLimit = 50f;
    private readonly float frontoffset = 20f;
    private readonly float frontoffset1 = 30f;
    private readonly float frontoffset2 = 60f;
    #endregion

    #region Public Methods
    public void ChangeLaneRelated()
    {
        //if(mainScript.targetNode!=null)
        //{
        //    //mainScript.canChangeLane = EnoughDistanceToChangeLane(mainScript.Reference, mainScript.targetNode);
        //    mainScript.wantToChangeLane = LaneChanger();
        //    ChangeLane();
        //}
    }

    public bool UpdateAbilityToChangeLane(GameObject nextLaneCandidatePoint, GameObject referencePoint)
    {
        string[] Lanes = new string[] { "Lane1", "Lane2", "Lane3", "Lane4" };
        if (nextLaneCandidatePoint != null)
        {
            if (Array.IndexOf(Lanes, referencePoint.tag) % 2 == 0)
            {
                return (Array.IndexOf(Lanes, nextLaneCandidatePoint.tag) == Array.IndexOf(Lanes, referencePoint.tag) + 1);
            }
            else
            {
                return (Array.IndexOf(Lanes, nextLaneCandidatePoint.tag) == Array.IndexOf(Lanes, referencePoint.tag) - 1);
            }
        }
        return false;
    }
    #endregion

    #region Private Methods
    private void ChangeLane()
    {
        //if (mainScript.wantToChangeLane && mainScript.canChangeLane)
        //{
        //    mainScript.wantToChangeLane = false;
        //    mainScript.canChangeLane = false; //You cant change lane before a turn
        //    mainScript.ChangingLane = true;
        //    mainScript.turningBehaviourScript.Turning = true;

        //    //mainScript.carPathGenerationScript.GenerateSpline(ChangeLanePoints(mainScript.Reference, mainScript.targetNode, mainScript.ChangeLaneTargetNode, mainScript.axisMovement, mainScript.positive));

        //    GameObject temp0;
        //    temp0 = mainScript.targetNode;
        //    mainScript.targetNode = mainScript.ChangeLaneTargetNode;
        //    mainScript.ChangeLaneTargetNode = temp0;
        //    temp0 = mainScript.nextNode;
        //    mainScript.nextNode = mainScript.ChangeLaneNextnode;
        //    mainScript.ChangeLaneNextnode = temp0;
        //}
    }

    private List<Transform> ChangeLanePoints(GameObject Ref, GameObject target, GameObject sideTarget, Vector3 axis, int IsPositive)
    {
        List<Transform> controlPoints1 = new List<Transform>();

        controlPoints1.Add(Ref.transform);

        Vector3 desiredCurrentTarget = new Vector3();
        Vector3 desiredCurrentTarget1 = new Vector3();
        Vector3 midPointCurrentLane = new Vector3();
        Vector3 sidePositionInFrontOfReference = new Vector3();
        Vector3 sideCurrentTarget = new Vector3();
        Vector3 sideCurrentTarget1 = new Vector3();

        Vector3 positionInFrontOfReference = Ref.transform.position + axis * (IsPositive) * frontoffset;

        if (axis == Vector3.right)
        {
            desiredCurrentTarget = positionInFrontOfReference + axis * (IsPositive) * frontoffset1;

            midPointCurrentLane = positionInFrontOfReference;
            midPointCurrentLane.x = (positionInFrontOfReference.x + desiredCurrentTarget.x) / 2f;

            sidePositionInFrontOfReference = positionInFrontOfReference;
            sidePositionInFrontOfReference.z = sideTarget.transform.position.z;

            sideCurrentTarget = desiredCurrentTarget;
            sideCurrentTarget.z = sideTarget.transform.position.z;

            desiredCurrentTarget1 = positionInFrontOfReference + axis * (IsPositive) * frontoffset2;

            sideCurrentTarget1 = desiredCurrentTarget1;
            sideCurrentTarget1.z = sideTarget.transform.position.z;
        }
        else if (axis == Vector3.forward)
        {
            desiredCurrentTarget = positionInFrontOfReference + axis * (IsPositive) * frontoffset1;

            midPointCurrentLane = positionInFrontOfReference;
            midPointCurrentLane.z = (positionInFrontOfReference.z + desiredCurrentTarget.z) / 2f;

            sidePositionInFrontOfReference = positionInFrontOfReference;
            sidePositionInFrontOfReference.x = sideTarget.transform.position.x;

            sideCurrentTarget = desiredCurrentTarget;
            sideCurrentTarget.x = sideTarget.transform.position.x;

            desiredCurrentTarget1 = positionInFrontOfReference + axis * (IsPositive) * frontoffset2;

            sideCurrentTarget1 = desiredCurrentTarget1;
            sideCurrentTarget1.x = sideTarget.transform.position.x;
        }

        GameObject Temp1 = Instantiate(target, positionInFrontOfReference, Quaternion.identity);
        GameObject Temp2 = Instantiate(target, sideCurrentTarget, Quaternion.identity);
        GameObject Temp3 = Instantiate(target, sideCurrentTarget1, Quaternion.identity);

        controlPoints1.Add(Temp1.transform);
        controlPoints1.Add(Temp2.transform);
        controlPoints1.Add(Temp3.transform);

        controlPoints1.Add(sideTarget.transform);


        Destroy(Temp1);
        Destroy(Temp2);
        Destroy(Temp3);


        return controlPoints1;
    }
    private bool LaneChanger()
    {
        if (Input.GetKeyDown("space")) return true;
        else return false;
    }

    private bool EnoughDistanceToChangeLane(GameObject Ref, GameObject target)
    {
        return (Ref.transform.InverseTransformPoint(target.transform.position).magnitude > changeLaneDistanceLimit);
    }


    #endregion
}
