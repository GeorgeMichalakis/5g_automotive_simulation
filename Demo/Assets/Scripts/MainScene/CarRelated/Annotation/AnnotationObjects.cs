﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnnotationObjects : MonoBehaviour
{
    public bool displayAnnotations = false;
    private void Update()
    {
        foreach (Transform child in this.transform) child.GetComponent<MeshRenderer>().enabled = displayAnnotations;
    }
}
