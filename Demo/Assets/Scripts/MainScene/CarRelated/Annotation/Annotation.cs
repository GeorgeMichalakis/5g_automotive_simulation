﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Annotation : MonoBehaviour
{
    #region Attributes
    public Data scriptData;
    private readonly float sensorLength = 100f;
    private float[] frontSensorAngles = new float[12];
    private float[] offFOV = new float[20];
    private LayerMask carLayer;
    private LayerMask pedestrianLayer;
    private float offset=0.5f;
    #endregion

    private void Awake()
    {
        //this.gameObject.SetActive(scriptData.enable5G);
        //this.enabled = scriptData.enable5G;
    }
    void Start()
    {
        carLayer = LayerMask.GetMask("Car");
        pedestrianLayer = LayerMask.GetMask("Pedestrian");
        SensorAngles();
    }

    void Update()
    {
        VisibleGameobjects(transform, sensorLength, frontSensorAngles, carLayer, pedestrianLayer);
    }

    #region Private Methods

    private void VisibleGameobjects(Transform refTransform, float sensorLen, float[] Angles, LayerMask layer1, LayerMask layer2)
    {
        for (int i = 0; i < Angles.Length; i++)
        {
            Vector3 origin = refTransform.position - Vector3.up * offset;
            RaycastHit hit;
            if (Physics.Raycast(origin, Quaternion.AngleAxis(Angles[i], refTransform.up) * refTransform.forward, out hit, sensorLen, layer1 | layer2))
            {
                if (hit.collider.gameObject.CompareTag("Car"))
                {
                    hit.collider.gameObject.GetComponentInChildren<AnnotationObjects>().displayAnnotations = true;
                    hit.collider.gameObject.GetComponentInChildren<AnnotationObjects>().gameObject.transform.rotation = Quaternion.LookRotation(transform.position - hit.collider.gameObject.transform.position);
                    hit.collider.gameObject.GetComponentInChildren<AnnotationObjects>().gameObject.transform.GetChild(0).gameObject.GetComponent<TextMesh>().text = "Speed: " + hit.collider.gameObject.GetComponent<Data>().currentSpeed.ToString("0") + " km/h";
                    hit.collider.gameObject.GetComponentInChildren<AnnotationObjects>().gameObject.transform.GetChild(1).gameObject.GetComponent<TextMesh>().text = "Distance: " + transform.InverseTransformPoint(hit.collider.gameObject.transform.position).magnitude.ToString("0");
                }
            }
        }
        for (int i = 0; i < offFOV.Length; i++)
        {
            Vector3 origin = refTransform.position - Vector3.up * offset;
            RaycastHit hit;
            if (Physics.Raycast(origin, Quaternion.AngleAxis(offFOV[i], refTransform.up) * refTransform.forward, out hit, sensorLen, layer1 | layer2))
            {
                if (hit.collider.gameObject.CompareTag("Car")) hit.collider.gameObject.GetComponentInChildren<AnnotationObjects>().displayAnnotations = false;

            }
        }
    }
    private void SensorAngles()
    {
        int temp = -30;
        for (int i = 0; i < frontSensorAngles.Length; i++)
        {
            frontSensorAngles[i] = temp;
            temp += 5;
        }
        temp = -40;
        for (int i = 0; i < offFOV.Length; i++)
        {
            if (i == 10) offFOV[i]=31;
            offFOV[i] = temp;
            temp += 1;
        }
    }
    #endregion
}
